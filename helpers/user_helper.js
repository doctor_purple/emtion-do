
//given an user, it builds a proper json to send back. 
//This will not contain any informaiton about password, and email
var user_helper = {
	reduceUserInfo: function(user, req) {
		var _user = {
			"user" : {
				"username": user.username,
				"id": user.id,
				"firstname": user.firstname,
				"lastname": user.lastname,
				"gender": user.gender,
				"scope": req.authInfo.scope
		}};

		return _user; //return user json object, with less info
	},

	reduceUsersInfo: function(users, req) {
		var _users = [] 
		users.forEach(function(user) {
			if (user) {
				var _user = user_helper.reduceUserInfo(user, req);
				_users.push(_user);
			}
		});

		return _users;
	}
}

module.exports = user_helper;



