var mongoose = require('mongoose'),
	Image = mongoose.model('Image'),
	ReEmotion = mongoose.model('ReEmotion'),
	emotions_list = require ('../config/config')['emotions'].emotions_list, 
	array_helper = require('../helpers/array_helper');



//GET ALL EMOTIONS===================================================================================
exports.getEmotionsList = function(req, res, next) {
	if (array_helper.checkArray(emotions_list)) {
		res.send(emotions_list);
	} else {
		res.send(404, 'Emotions list not found');
	}
}

//RE-EMOTIONATE A PICTURE============================================================================
exports.addReEmotionToImage = function(req, res, next) {
	//console.log(req.user);
	var user_id = req.user.userId;
	var image_id = req.params.image_id;
	var emotion = req.body.emotion;

	if (!user_id || !image_id || !emotion) { //check that all the input values exist
		res.send(500, 'Error in the requesting fields, check them');
	} else {	
		 Image.findById(image_id, function (err, image) {
		 	if (image) {
		 		//push the object Re-emotion at the end of the array
		 		image.re_emotions.push({
		 			emotion: emotion, 
		 			userId: user_id});
		 
		 		image.save(function(err) {
		 			if(!err) res.send(image);
		 			else return next(err);
		 		});
		 	} else {
		 		return next(err);
		 	}
		 });		
	}
}

//GET ALL RE-EMOTIONS FOR AN IMAGE====================================================================
exports.getReEmotionsOfImage = function (req, res, next) {
	var image_id = req.params.image_id;
	var limit = req.query.limit || 10;

	if (image_id) {
	Image.findById(image_id, function (err, image) {
			if (image) {
				res.send(image.re_emotions);
			} else {
				return next(err);
			}
		});
	} else {
		return res.send(500);
	}
}


//DELETE re-emotion, only if the user is the same who did the operation==================================
exports.removeReEmotion = function (req, res, next) {
	var image_id = req.params.image_id;
	var re_emotion_id = req.params.re_emotion_id;
	var user_id = req.user.userId

	if (image_id && re_emotion_id && user_id) {
		Image.findById(image_id, function (err, image) {
			if (image) {
				var re_emotion = image.re_emotions.id(re_emotion_id); //object re_emotion, retrieved with the id
				
				if (re_emotion.userId === user_id) {
					image.re_emotions.pull(re_emotion_id); //remove the re-emotion with the id from the sub-doc array
					
					image.save(function(err) {
	 					if(!err) res.send(image);
	 					else return next(err);
		 			});
				} else return res.send(401); //not authorized to modify 
			} else {
				return next(err);
			}
		});
	} else {
		return res.send(500);
	}
}

