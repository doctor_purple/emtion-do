var mongoose = require('mongoose'), 
	Image = mongoose.model('Image'), 
	events = require('events'),
	EventEmitter = events.EventEmitter , //emitter of events, improving of the readability of nested callbacks
	fs = require('fs'),  //node file module
	path = require('path'),
	gm = require('gm'); //imagemagick wrapper tools for manipulating images

var thumbWidth = 256; //the thumbnail widht to be scaled, maintaing the aspect ratio 
var flowController = new EventEmitter(); //the flow controller is used to make the operation completely asynchronous and sequential at the same time
var mimeTypes = {
	"jpg": "image/jpeg",
	"png": "image/png",
	"jpeg": "image/jpeg"
};





//UPLOAD A NEW IMAGE IN THE SYSTEM, using a flow controller in sequential way=======================================================================
exports.uploadImage = function (req, res, next) {
	flowController.on('start', function() {
		readUploadFile(req, res, next);
	});
	
	flowController.on('readUploadFile_done', function (fullsizePath, thumbsizePath, emotion, data) {
		writeImages(req, res, next, fullsizePath, thumbsizePath, emotion, data);
	});

	flowController.on('imagesCreated_done', function (image) {
		storeImage(req, res, next, image);
	});

	flowController.emit('start');
}


//GET the image fullsize, given the id of the thumbnail=============================================================================================
exports.getFullsizeImage = function (req, res, next) {
	Image.findById(req.params.image_id, function(err, image) {
		if (image) {
			fileStreamImage(req, res, next, image); //write the image in the stream, build the header the returns the data
		} else {
			console.log(err);
			res.send(404, 'Image not found');
			return;
		}
	});
	
}

//GET images of the user (via id), with a query limit and ordered by descending time of creation======================================================
exports.getImagesOfUser = function(req, res, next) {
	var userId = req.user.userId;
	var limit = req.query.limit || 12;

	var query = Image.find({userId: userId}).sort({created_at: 'desc'}).limit(limit);
	query.exec(function(err, images) {
		if (images) {
			res.send(images);

		} else {
			res.send(404, 'Images not found for this user');
			return;
		}
	});

}



//CALLBACK FUNCTIONS==================================================================================================================================
/**
 * read the uploaded file, extract the paths and emits a message for the other methods
 */
function readUploadFile (req, res, next) {
	fs.readFile(req.files.image.path, function (err, data) {
		var imageName = req.files.image.name;
		var emotion = req.body.emotion;

		//if there's an error
		if(!imageName || !emotion) {
			var error_message = "error in uploading the image";
			res.send(500, error_message); //end of the response
		} else {
			var fullsizePath = __dirname + "/upload/fullsize/" + imageName;
			var thumbsizePath = __dirname + "/upload/thumbnail/" + imageName;
			flowController.emit('readUploadFile_done', fullsizePath, thumbsizePath, emotion, data); //the emitter sends the message and the objects to next block
		}	
	});
}

/**
 * Write images, before the fullsize and then the thumbnail. If there are not errors, the image schema object is created and is ready to be saved in mongodb
 */
function writeImages(req, res, next, fullsizePath, thumbsizePath, emotion, data) {
	//WRITING files, with fullsize and thumbnail. For now, only in local on the upload folder. The idea is to save them in S3 amazon static storage service
	fs.writeFile(fullsizePath, data, function(err, result){ //write the fullsize file, and then it creates the thumbnail
		if (err) return next(err);

		//imagemagick resizing thumbnail
		gm(fullsizePath)
			.resize(thumbWidth)
			.write(thumbsizePath, function(err){
				if (err) return next(err);
			});

		//creation of the object image to put in the db
		var image = new Image({url_fullsize: fullsizePath, 
									url_thumbsize: thumbsizePath,
									emotion: emotion, 
									userId: req.user.userId});
		
		flowController.emit('imagesCreated_done', image); //when the two image have been stored and the image object has been created
	});
}

/**
 * Save the image object returned from the previous block
 */
function storeImage(req, res, next, image) {
	image.save(function(err, success) {
		if (success) {
			res.send(201, success);
		}
		else {
			return next(err);
		}
	});	
}

/**
 * Given the id, reads the file and return the binary stream of the image
 */
function fileStreamImage(req, res, next, image) {
	var url = image.url_fullsize;
			fs.exists(url, function(exists) {
				if (!exists) {
					console.log("image not exist");
					res.send(404, 'Image not found');
					res.end();
					return;
				} else {
					var mimeType = mimeTypes[path.extname(url).split(".")[1]];
					res.writeHead(200, {'Content-Type': mimeType});
				
					var fileStream = fs.createReadStream(url);
					fileStream.pipe(res);
				}	
			});

}

		
