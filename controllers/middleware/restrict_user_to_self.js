function restrictUserToSelf (req, res, next) {
	if (req.user.username != req.params.username) {
		res.send('Unauthorized', 401);
	} else {
		next();
	}
}


module.exports = restrictUserToSelf;