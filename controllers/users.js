
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	User = mongoose.model('User'), 
	Client = mongoose.model('Client'), //the client model is used to obtain the token
	user_helper = require('../helpers/user_helper');

//GET with a saved token in the phone
exports.login = function(req, res) {
	res.send(200);
}


exports.signup = function (req, res) {
	res.send(201);
}

//CREATE A NEW USER IN THE DATABASE ======================================================================================================================================
exports.createUser = function(req, res, next) {
	var user = new User(req.body); //create a new User instance with the data received from the POST request
	user.save(function(err, success) {	// try to save the User into the DB
		console.log('Saving User... Success: ' + success + ', Error: ' + err );
		if (success) {
			res.send(201);
			return next(); //unlock all the processes that were waiting the creation of the User
		} else {
			return next(err);
		}
	});
}

//the client is needed for get the access token==========================================================================================================================
exports.createClient = function(req, res, next) {
	//var client = new Client(req.body); this takes the params from the request
	var client = new Client ({name: 'Our iOS service version1', clientId: "mobileV1", clientSecret: "emotionando"});
	client.save(function(err, client) {
		if (client) {
			res.send(201);
		} else {
			return next(err);
		}
	});
}

//get all the user in the db, given some parameters in the request=======================================================================================================
exports.getAllUsers = function(req, res, next) {
	var limit = req.query.limit || 30;
	//var ordered_by = req.query.ordered_by

	var query = User.find({}).sort({'username': 1}).limit(limit); 
	query.exec(function(err, users) {
		if (users) {
			users = user_helper.reduceUsersInfo(users, req); //replace the users array with an array of object modified
			res.send(users);
		} else {
			return next(err);
		}
	});
}

//get the user info given the username====================================================================================================================================
exports.getUserByUsername = function(req, res, next) {
	var username = req.params.username;
	User.findOne({username: username}, function(err, user){
		if (user) {
			res.send(user_helper.reduceUserInfo(user));
		} else {
			return next(err);
		}
	});
}

//delete the user in the db================================================================================================================================================
exports.deleteUserByUsername = function(req, res, next) {
	var username = req.params.username;
	console.log(username);
	User.findOne({username: username}).remove(function(err, result){
		if (result) {
			res.send(200);
		} else {
			return next(err);
		}
	});
}

//update the the profile of the user, given the username====================================================================================================================
exports.updateUserByUsername = function(req, res, next) {
	var username = req.params.username;
	User.findOneAndUpdate({username: username}, req.body, function(err, result) {
		if (result)
			res.send(201, 'Updated');
		else
			return next(err);
	});
}

