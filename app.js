/**
 * Module dependencies
 */

var application_root = __dirname,
	express = require('express'), //server utilities
	http = require('http'),
	https = require('https'),
	fs = require('fs'), //file system utilities
	passport = require('passport');


/**
 * Main application entry file. The order of loading is important
 */

//Load configuration
var env = process.env.NODE_ENV || 'development'
	config = require('./config/config')[env],
	mongoose = require('mongoose');

//Bootstrap db connection
//Connect to mongodb
var connect = function(){
	var options = {server: {socketOptions: {keepAlive: 1}}}
	mongoose.connect(config.db, options);
}
connect();






//--------------------LISTENERS ON MONGOOSE EVENTS------------------------
//Error handler
mongoose.connection.on('error', function(err) {
	console.log("Mongodb error: " + err);
});

//Reconnect when mongo is closed
mongoose.connection.on('disconnected', function() {
	console.log("Mongodb is disconnected") 
	// if disconnected try to make it start from console

});

//Only if the mongoose returns a good values I start doing the boostrapping
//and starting the server 

mongoose.connection.on('connected', function(err) {
	console.log("Mongoose is now connected");

	//Load models
	var models_path = __dirname + '/models'
	fs.readdirSync(models_path).forEach(function (file) {
		if (~file.indexOf('.js')) {
			require(models_path + '/' + file);
		}
	});



	//Express settings
	var app = express();
	require('./config/express')(app, config, passport);

	//Load routes from configuration files
	require('./config/routes')(app, passport)

	//HTTPS certificate =============================================================
	//For creating the HTTPS using the TLS protocol over HTTP
	//private key and public certificate
	var privateKey = fs.readFileSync('./certificate/emotion_key.pem', 'utf-8'), 
		certificate = fs.readFileSync('./certificate/emotion_cert.pem', 'utf-8');
	var credentials = {key: privateKey, cert: certificate};
	var httpServer = http.createServer(app);
	var httpsServer = https.createServer(credentials, app);

	//start the server on port 8080, and 3000 (in deployment should be
	// 443 but it creates conflicts to the default port of the other
	// https process
	httpServer.listen(9000,'0.0.0.0', function(){
		console.log("HTTP server created on port " + httpServer.address().port);
	});
	
	httpsServer.listen(3000,'0.0.0.0', function(){
		console.log("HTTPS server created on port " + httpsServer.address().port);
	});

	exports = module.exports = app;
		
});



