var mongoose = require('mongoose'), 
	validate = require('mongoose-validator').validate,
	Schema = mongoose.Schema, 
	crypto = require('crypto'); //for crypting and decrypting the token 

/**
 * Client Schema for mongo db
 * contains the client application of the user, with name and access code
 */
 //Client schema ==================================================================
var ClientSchema = new Schema({
	name :  {
		type: String, 
		unique: true, 
		required: true
	},

	clientId: {
		type: String, 
		required: true
	},

	clientSecret: {
		type: String,
		required: true
	}
});




/**
 * Authentication token is an object linked to the client application,
 * with a limit of time
 */
 //AuthToekn token schema ==========================================================
var AccessToken = new Schema ({
	userId: {
		type: String, 
		required: true
	}, 

	clientId: {
		type: String, 
		required: true
	},

	token : {
		type: String, 
		unique: true,
		required: true
	},
	
	created_at: {
		type: Date,
		default: Date.now
	}

});



/**
 * Refresh token is another type of token which allows the client to request a new token without
 * asking the password to the user another time
 */
 //Refresh token schema ==========================================================
 var RefreshToken = new Schema ({
 	userId: {
 		type: String, 
 		required: true
 	},

 	clientId: {
 		type: String,
 		required: true
 	},

 	token: {
 		type: String, 
 		required: true, 
 		unique: true
 	},

 	created_at: {
 		type: Date,
 		default: Date.now
 	}
 });



//assignation ===================================================================
 mongoose.model('Client', ClientSchema);
 mongoose.model('AccessToken', AccessToken);
 mongoose.model('RefreshToken', RefreshToken)
