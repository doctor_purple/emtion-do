var mongoose = require('mongoose'), 
	validate = require('mongoose-validator').validate,
	Schema = mongoose.Schema, 
	crypto = require('crypto'); //for crypting and decrepting the password and the token

/**
 * Validators using mongoose-validator
  */
var alphaNumericValidator = [validate({message: "String should be between 3 and 50 characters"},'len', 3, 50), validate('isAlphanumeric')];
var emailValidator = [validate({message: "String should be between 3 and 64 characters"},'len', 5, 64), 
					  validate({message: "Email format not valid"},'isEmail')];


/**
 * User Schema for mongo db
 */
 //User schema ==============================================================================
var UserSchema = new Schema({
	email: {type: String, 
			required: true, 
			index: {unique: true, sparse:true }, //sparse: index maintained in insertion order
			validate: emailValidator},

	hashed_password: {type: String, 
					  required: true}, 

	salt: {type: String, 
		   default: ''}, //token will be concatened with the hased password

	username: {type: String, 
			   required: true,
			   index: {unique: true, sparse: true },
			   validate: alphaNumericValidator},

	firstName: {tpye: String, 
			    default: ''},

	lastName: {type: String, 
			   default: ''}, 

	birthDate: {type: String, 
				default: ''},

	gender: {type: String, 
			 default: ''}, //type enum: Male/Female

	created_at: {type: Date, 
				 default: Date.now, set: function(val) {
					return undefined;}},

	updated_at: {type: Date, 
			     default: Date.now}
});


/**
 * Virtuals: attributes that you can get and set but that are not themselves persisted to mongodb
 * In that case, the mapping of Mongoose uses password. When a password is set, indeed, the other two
 * values are created: hash_password and salt
 * 
 */

UserSchema
	.virtual('password') // attribute will not be persistent to mongodb
	.set(function(password){
		this._password = password;
		this.salt = this.makeSalt();
		this.hashed_password = this.encryptPassword(password);
	})
	.get(function(){ return this._password});

UserSchema.virtual('userId')
		  .get(function(){
		  	return this.id; //return the id before saving
		  });


/**
 * The .pre is executed before the event 'save'. 
 * Perfect place for chaingin or creating value before
 * putting in the db
 */


var validatePresenceOf = function (value) {
  return value && value.length
}
/*
 * validator on lenght and alphanumeric
 */


/*
 * next() commands says to the process waiting that can be done
 */
UserSchema.pre('save', function(next){
	if (!this.isNew) return next();
	else created_at = undefined; //avoids that the user could overrwrite through GET request

	if (!validatePresenceOf(this.password)) next(new Error('Invalid Password'));
	else {
		console.log("pre-save phase" + this.password);
		next(); //in this case next calls the virtuals (on the fly operation) on password
	}
});



//User methods ====================================================================================
UserSchema.methods = {

	/**
	 * checkPassword - check if the passwords are the same
	 *
	 * @param {String} plainText
	 * @return {Boolean}
	 * @api public
	 */

	checkPassword: function (plainText) {
		return this.encryptPassword(plainText) === this.hashed_password;
	}, 

	/**
  	 * Make salt
     *
     * @return {String}
     * @api public
     */

  	makeSalt: function () {
    	return Math.round((new Date().valueOf() * Math.random())) + ''
  	},

  	/**
     * Encrypt password using sha, message digest from a lenght variable string. 
     *
     * @param {String} password
     * @return {String}
     * @api public
     */

  	encryptPassword: function (password) {
    	if (!password) return '';
    	var encrypred;
   
   		try {
      		encrypred = crypto.createHmac('sha1', this.salt).update(password).digest('hex')
      		return encrypred;
    	} catch (err) {
      	return '';
    	}
  	}
}


//accessible mongoose from outside
mongoose.model('User', UserSchema);
