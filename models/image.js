var mongoose = require('mongoose'), 
	validate = require('mongoose-validator').validate,
	Schema = mongoose.Schema, 
	emotions_list = require ('../config/config')['emotions'].emotions_list;


/**
 * Validators using mongoose-validator, which is built on top of module validator.js.
 * https://github.com/chriso/validator.js to have more info about the validations can be
 * performed
 */
var alphaNumericValidator = [validate({message: "String should be between 3 and 50 characters"},'len', 3, 50), validate('isAlphanumeric')];
var urlValidator = validate('isUrl'); 



/**
 * ReEmotion schema for mongo db, in order to put it inside 
 */
 //ReEmotion Schema===============================================================================
var ReEmotionSchema = new Schema({
	emotion: {type: String, enum: emotions_list, validate: alphaNumericValidator},

	userId: {type: String}, //the required filed does not work properly, check the esistence on the controller side

	created_at: {type: Date, default: Date.now}

});


/**
 * Images schema for mongo db
 */
 //Image schema===============================================================================


 var ImageSchema = new Schema({
 	url_fullsize: { type: String, required: true}, //urlValidator, when it will be a url

 	url_thumbsize: {type:String, required: true}, //urlValidator, when it will be a url

 	emotion: {type: String, required: true, enum: emotions_list, validator: alphaNumericValidator},

 	re_emotions: [ReEmotionSchema], //array of re-emotion objects

 	userId: {type: String, required: true}, 

 	created_at: {type: Date},

	updated_at: {type: Date}

 });





//PRE-SAVING CHECK==============================================================================
ImageSchema.pre('save', function(next) {
	var now = new Date();
	
	this.updated_at = now;
	if (!this.created_at) {
		this.created_at = now;
	}
	next(); //call the following mehtod, in this case save
});


//external exports
mongoose.model('Image', ImageSchema);
mongoose.model('ReEmotion', ReEmotionSchema);