#curl POST for a new username
curl -i  -k -X POST -H "Content-Type: application/json" -d \
	'{"username": "allegriaaaa", 
	  "firstname":"daniele", 
	  "lastname":"battaglino", 
	  "email":"battaglino@hotmail.it", 
	  "birthDate":"15/04/1989", 
	  "gender":"male", 
	  "password": "ciao"}' \
 https://127.0.0.1:3000/users

# curl for https
curl -i -X GET https://127.0.0.1:3000/login

#httpie for having the access token and the refresh token
http --verify=no POST https://localhost:3000/oauth/token \
grant_type=password client_id=mobileV1 client_secret=emotionando \
username=allegriaaaa password=ciao

#httpie for having another access token given the refresh one
http --verify=no POST https://localhost:3000/oauth/token grant_type=refresh_token client_id=mobileV1 client_secret=emotionando refresh_token=dNuMHoVkylwlNJF5/OAOR454qOTRsVMvg9ZG1DrumvU=

#httpie for knowing the user info given the access token
http --verify=no https://localhost:3000/api/userinfo Authorization:'Bearer V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg='

#httpie for knowing if the api is running
http --verify=no https://localhost:3000/api Authorization:'Bearer V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg='

#httpie for knowing the user info given the access token: the double == is used for passing params directly inside the url
http --verify=no https://localhost:3000/users limit==20

#httpie for deleting an user
http --verify=no DELETE  https://localhost:3000/users/:username

#httpie for updating an user
http --verify=no PUT https://localhost:3000/users/allegriaaaa firstname=otto

#upload of a new picture
http --verify=no -f POST https://localhost:3000/upload  access_token=V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg= image@~/Immagini/foto.jpg emotion=happyness

#http get request for getting the fullsize of an image given the id
http --verify=no https://localhost:3000/images/5314d6af2db7f3f4191c4a08 Authorization:'Bearer V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg=' 

#get request for getting all the images of a user
http --verify=no https://localhost:3000/images Authorization:'Bearer V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg=' 

#add an re-emotion to an image already created
http --verify=no PUT https://localhost:3000/images/5314d6af2db7f3f4191c4a08/re_emotions access_token=V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg= emotion=happyness

#get all the re-emotions for an image
http --verify=no https://localhost:3000/images/5314d6af2db7f3f4191c4a08/re_emotions

#remove a specific re-emotion
http --verify=no DELETE https://localhost:3000/images/5314d6af2db7f3f4191c4a08/re_emotions/53150449a79f772c32e2eed1 access_token=V5rKWLuQ0BLsU+ffi0TOPU/oD89b2+/GP0iPdv/Qxzg= 


