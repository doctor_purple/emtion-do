var should = require('should'), //error message readibility
	assert = require('assert'), 
	request = require('supertest'), //HTTP assertion  
	mongoose = require('mongoose'), //mondodb driver 
	winston = require('winston'), //logfile writer
	config = require('../config/config')['development']; //configuration of debug

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" //allows to do the https without an official certificate


describe('Routing', function() {
	var host = config.app.host, 
		secure_host = config.app.secure_host,
		http_port = config.app.http_port,
		https_port = config.app.https_port,
		http_port = config.app.http_port;
		


	//within before I can run all the setup required to set up
	//my test. Once done() is called, it will start the test
	//before(function(done){
		//we could change the connection to another db, like 'test'
		//check if the server are running or not

		//console.log(obj);
		//app.listen(8080);
		//app.listen(3000);
		// appInstance.on('connection', done()); //call the done() after the connecting phase
	 //});

	it('GET /login should return 200', function(done){
		request(secure_host+":"+https_port) //reject unauthorized (in this case because the certificate is not checked
				.get('/login')
				.end(function(err, res){
					if (err) {
						throw err;
					}
					res.should.have.status(200);
					done();
			});
	});
});