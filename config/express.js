/**
 *Module dependencies
 */
var express = require('express'),
	winston = require('winston'),
	pkg = require('../package.json');

var env = 'development';


module.exports = function (app, config, passport) {
	app.set('showStackError', true)

	//Logging, using winston ================================================
	var log;
	if (env !== 'development') {
		log = {
			stream: {
				write: function (message, encoding) {
					winston.info(message);
				}
			}
		}
	} else {
		log = 'dev';
	}
	//no logging during testing
	if (env !== 'test') app.use(express.logger(log));


	
	//developmente env config
	app.configure(function(){
		//express configuration ==================================================
		//app.use(express.cookieParser()); // cookieParser should be above session
		app.use(express.bodyParser()); // bodyParser should be above methodOverride
		//passport configuration =====================================================
		app.use(passport.initialize());
		app.use(express.methodOverride()); // bodyParser should be above methodOverride
		//routes ======================================================================
		app.use(app.router);
	});
}
