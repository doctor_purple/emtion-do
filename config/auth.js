//--------------PASSPORT CONFIGURATION----------------------
//----------------------------------------------------------
//In this file are described the passport stategies to be
//loaded when passport module is called in express

var config = require('./config')['security'],
	BasicStrategy = require('passport-http').BasicStrategy, //authentication of HTTP requests using a stretegy
	//The basic authentication uses a userid and a password. It requires a callback, which accepts this callback
	//and calls done providing an user
	ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy, //authenticate request containg client credentials
	// as defined in the oauth2 specification (with a client ID and client secret token)
	BearerStrategy = require('passport-http-bearer').Strategy, //strategy for bearer token, the token is just enough for accessing the 
	//resources, without demonstrating that the client owns a cryptographic key . They required an high level of protection from disclosure
	mongoose = require('mongoose'),
	UserModel = mongoose.model('User'),
	ClientModel = mongoose.model('Client'),
	AccessTokenModel = mongoose.model('AccessToken'),
	RefreshTokenModel = mongoose.model('RefreshToken');


	module.exports = function(passport) {
		/*
		 * Three different way of authentication, which can be used separately or combined
		 */

		// USERNAME & PASSWORD authentication====================================================================================================
		passport.use(new BasicStrategy(
			function(username, password, done){
				ClientModel.findOne({clientId: username}, function(err, client){
					if (err) {return done(err);}
					if (!client) {return done(null, false);}
					if (client.clientSecret != password) {return done(null, false);}

					return done(null, client);
				});
			}
		));


		// CLIENTID & CLIENT SECRET==============================================================================================================
		passport.use(new ClientPasswordStrategy(
			function(clientId, clientSecret, done) {
				ClientModel.findOne({ clientId: clientId}, function(err, client){
					if (err) { return done(err);}
					if (!client) { return done(null, false);}
					if (client.clientSecret != clientSecret) { return done(null, false);}

					return done(null, client);
				});
			}
		));

		//ONLY CHECK THE ACCESS TOKEN with the expiration date=============================================================================================================
		passport.use(new BearerStrategy(
			function(accessToken, done) {
				AccessTokenModel.findOne({token: accessToken}, function(err, token) {
					if (err){ return done(err);}
					if (!token) { return done(null, false);}

					if (Math.round((Date.now() - token.created_at)/1000) > config.tokenLife) {
						AccessTokenModel.remove({token: accessToken}, function (err){
							if (err) return done(err);
						});
						return done(null, false, {message: 'Token expired'});
					}

					UserModel.findById(token.userId, function(err, user) {
						if (err) { return done(err); }
						if (!user) { return done(null, false, {message: 'Unknown user'});}

						// to keep this example simple, restricted scopes are not implemented,
	          			// and this is just for illustrative purposes
						var info = {scope: '*'}; //global scope
						done(null, user, info);
					});
				});
			}
		));
	}
	

