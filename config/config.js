var path = require('path'), 
	rootPath = path.normalize(__dirname + '/..'),
	app_name = 'emotion&do';


module.exports = {
	security: {
		tokenLife: 360000000000},

	emotions : {emotions_list: ["happyness", "sadness", "boredom", "excitement"]},

	development: {
		db: 'mongodb://localhost:27017/' + app_name, 
		root: rootPath, 
		app: {
			name: app_name
		}
	},

	test: {},

	production: {} 
}