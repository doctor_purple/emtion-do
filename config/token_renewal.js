var oauth2orize = require('oauth2orize'), 
	passport = require('passport'),
	crypto = require('crypto'),
	security = require ('./config')['security'],
	mongoose = require('mongoose'),
	UserModel = mongoose.model('User'),
	ClientModel = mongoose.model('Client'),
	AccessTokenModel = mongoose.model('AccessToken'),
	RefreshTokenModel = mongoose.model('RefreshToken');


	//Create oauth 2.0 server=====================================================================================================
	//This server is in charge to listen for token renewal. In the routes.js, when the access token is expired, the client can ask 
	//for a new one by using the 'oauth2.token' override method. In combination with the client information, it is possible to re-new
	//the tokens


	var server = oauth2orize.createServer();
	//Exchange username and password==============================================================================================
	server.exchange(oauth2orize.exchange.password( function(client, username, password, scope, done){
		UserModel.findOne({username: username}, function(err, user) {
			if (err) { return done(err);}
			if (!user) { return done(null, false);}
			if (!user.checkPassword(password)) {return done(null, false);}
			

			RefreshTokenModel.remove({userId: user.userId, clientId: client.clientId}, function(err) {
				if (err) return done(err);
			});
			AccessTokenModel.remove({userId: user.userId, clientId: client.clientId}, function(err) {
				if (err) return done(err);
			});

			var tokenValue = crypto.randomBytes(32).toString('base64');
			var refreshTokenValue = crypto.randomBytes(32).toString('base64');
			var token = new AccessTokenModel({token: tokenValue, clientId: client.clientId, userId: user.userId });
			var refreshToken = new RefreshTokenModel({token: refreshTokenValue, clientId: client.clientId, userId: user.userId});
			refreshToken.save(function (err) {
				if (err) return done(err);
				else{
					var info = {scope: '*'};
					token.save(function(err, token) {
					if (err) { return done(err);}
						done(null, tokenValue, refreshTokenValue, {'expires_in': security.tokenLife});

					});
				}
			});
		});
	}));


	//Exchange refresh token for access token======================================================================================
	server.exchange(oauth2orize.exchange.refreshToken( function (client, refreshToken, scope, done){
		RefreshTokenModel.findOne({ token: refreshToken}, function(err, token) {
			if (err) { return done(err);}
			if (!token) {return done(null, false);}

			UserModel.findById(token.userId, function(err, user) {
				if (err) {return done(err);}
				if (!user) {
					return done(null, false);
				}

				RefreshTokenModel.remove({ userId: user.userId, clientId: client.clientId}, function(err) {
					if (err) return done(err);
				});

				AccessTokenModel.remove({userId: user.userId, clientId: client.clientId}, function(err){
					if (err) return done(err);
				});

				var tokenValue = crypto.randomBytes(32).toString('base64');
            	var refreshTokenValue = crypto.randomBytes(32).toString('base64');
            	var token = new AccessTokenModel({ token: tokenValue, clientId: client.clientId, userId: user.userId });
            	var refreshToken = new RefreshTokenModel({ token: refreshTokenValue, clientId: client.clientId, userId: user.userId });
            	refreshToken.save(function (err) {
                	if (err) { return done(err); }
            	});
            	
            	var info = { scope: '*' }
            	token.save(function (err, token) {
                	if (err) { return done(err); }
                	else {
                		done(null, tokenValue, refreshTokenValue, { 'expires_in': security.tokenLife});
                	}
            	});
			});
		});
	}));


	//TOKEN endpoint
	exports.token = [
		passport.authenticate(['basic', 'oauth2-client-password'], {session: false}),
		server.token(),
		server.errorHandler()
	]