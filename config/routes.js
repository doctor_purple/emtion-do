/**
 * Module dependencies
 */
 var async = require('async'),
  oauth2 = require('./token_renewal'), 
  user_helper = require('../helpers/user_helper'),
  restrictUserToSelf = require('../controllers/middleware/restrict_user_to_self');


 /**
 * Controllers: add the controllers location
 */

 var users = require('../controllers/users'),
 	images = require('../controllers/images'),
 	emotions = require('../controllers/emotions');

 /**
  * Expose routes
  */ 

  module.exports = function (app, passport) {
    require('./auth')(passport); // pass passport as parameter for the initialization



    //AUTHENTICATION APIS==================================================================================================================================
  	app.post('/oauth/token', oauth2.token); //see the curl httpie routes example to understand the params to use. In this case 
    app.get('/api/userInfo', 
     passport.authenticate('bearer', {session: false}), 
        function(req, res) {
          //req.authInfo is set using the 'info' argument
          //supplied by bearer Strategy. It is tipically used to 
          //indicate scope of the token, and used in access control 
          //checks. 
          res.json(user_helper.reduceUserInfo(req.user, req));
      }
    );

    app.get('/api', passport.authenticate('bearer', {session: false}), function(req, res){
      res.send('API is running with HTTPS and tokens authentication system!');
    });


    //USER APIS==============================================================================================================================================
  	//user routes: maps the routing with the methods
  	app.get('/login', passport.authenticate('bearer', {session: false}), users.login);
  	app.get('/signup', passport.authenticate('bearer', {session: false}), users.signup);
  	app.post('/users', passport.authenticate('bearer', {session: false}), users.createUser);
    app.get('/users', passport.authenticate('bearer', {session: false}), users.getAllUsers);
    app.get('/users/:username', passport.authenticate('bearer', {session: false}), users.getUserByUsername);
    app.get('/clients', passport.authenticate('bearer', {session: false}), users.createClient); //should be POST and receives the params from the https request 
    app.del('/users/:username', passport.authenticate('bearer', {session: false}), restrictUserToSelf, users.deleteUserByUsername);
    app.put('/users/:username', passport.authenticate('bearer', {session: false}), restrictUserToSelf, users.updateUserByUsername);
  

    //IMAGE APIS=================================================================================================================================================
    //images routes
    app.post('/upload', passport.authenticate('bearer', {session: false}), images.uploadImage);
    app.get('/images/:image_id', passport.authenticate('bearer', {session: false}), images.getFullsizeImage);
    app.get('/images', passport.authenticate('bearer', {session: false}), images.getImagesOfUser);


    //EMOTIONS LIST APIS==========================================================================================================================================
    app.get('/emotions', emotions.getEmotionsList);
    app.put('/images/:image_id/re_emotions', passport.authenticate('bearer', {session: false}), emotions.addReEmotionToImage); //update an image already created with an re-emotion object
    app.get('/images/:image_id/re_emotions', emotions.getReEmotionsOfImage);
  	app.del('/images/:image_id/re_emotions/:re_emotion_id', passport.authenticate('bearer', {session: false}), emotions.removeReEmotion);

  }
