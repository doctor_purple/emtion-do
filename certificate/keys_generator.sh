#TLS implementation, public and private key to crypt the message
#between client and server

#generating a private key for the server
openssl genrsa -out emotion_key.pem 1024

#certificate signing request
openssl req -new -key emotion_key.pem -out emotion_csr.pem

#generating a public key (self signed)
openssl x509 -req -in emotion_csr.pem -signkey emotion_key.pem \
-out emotion_certificate.pem
